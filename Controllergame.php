<?php
    class Controllergame {
        private $model;
        
        //konstruktorissa luodaan myös modelgame
        
        public function __construct() {
            
            $this->model = new Modelgame();
        }
        
        //"käynnistää" viewgame.php:n

        public function list_it() {
            include("Viewgame.php");
            
        }
        
        //EI KÄYTÖSSÄ

        // public function send() {
        //     $_SESSION["data"] = $_POST["password"];
        //     $_SESSION["datamessage"] = $_POST["user"];
        //     //$this->model->add_user($_SESSION["datamessage"], $_SESSION["data"]);
        //     header("Location: Logingame.php?action=list_it");
        
        
        //hakee korkeimmat pisteet databasesta ja palauttaa taulukon
        
        public function get_scores() {
            $this->model->get_scores();
        }
        
        //Hakee käyttäjän user pisteet, vertaa niitä uusiin pisteisiin ja
        //jos uudet on korkeammat niin päivittää databasen
        
        public function get_score($user, $score) {
            
            $array = json_decode($this->model->get_score($user),true);
            if ($array["score"]>=$score){
                echo $this->model->get_score($user);
            } else{
                $this->model->add_score($user, $score);
                echo $this->model->get_score($user);
            }
        }
        //hakee käyttäjän databasesta, jos ei löydy, niin luo uuden ja palauttaa käyttäjän nimen, jos löytyy niin vertaa salasanoja.
        //Jos salasana oikein, niin palauttaa käyttäjän nimen. Jos salasana väärin, niin palauttaa viestin väärästä käyttäjätunnuksesta/salasanasta
        
        public function get_user($user, $pass) {
            
            $message = "Username and/or Password incorrect.";
            if (empty($user) or empty($pass)){
                echo $message;
            } else {
                if (empty($this->model->get_user($user))){
                    $this->model->add_user($user, $pass);
                    $_SESSION["name"] = $user;
                    $name = $_SESSION["name"];
                    echo $name; 
                }
                else{
                    $array = json_decode($this->model->get_user($user), true);
                    if ($array["password"]==$pass){
                        $_SESSION["name"] = $user;
                        $name = $_SESSION["name"];
                        echo $name;
                    
                    } else {
                        echo $message;
            }
        }
        
      }  
     
    }  
    }
    
    //url pyynnöstä otetaan parametrit talteen eli tämä on ns RESTi
    
    include("Modelgame.php");
    
   
    
    $action = $_GET['action'];
    $q = $_GET['q'];
    $score = $_GET['score'];
    $pass = $_GET['pass'];
    $q = strip_tags($q);
    $score = strip_tags($score);
    $pass = strip_tags($pass);
  
    $controller = new Controllergame();
    
    //if lauseessa tarkistetaan url pyyntö ja toimitaan actionin mukaan
    
    
       
            
            if ($action == "get_user") {
            
            echo $controller->get_user($q, $pass);
            
            
            }
            elseif ($action == "get_score") {
                echo $controller->get_score($q, $score);
            }
            elseif ($action == "get_scores") {
                echo $controller->get_scores();
            }
       
        

    
    ?>
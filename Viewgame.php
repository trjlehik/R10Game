

<!DOCTYPE html>  
<html>
   <head>
      <title>Xmas games</title>
      <meta charset="utf-8" />
      <!--Tiedostot-->
      <!--BOOTSTRAP-->
      <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
      <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
      <!--OMAT-->
      <script src="kirjaudu.js"></script> 
      <script type="text/javascript" src="flappybird/js/phaser.min.js"></script>
      <script type="text/javascript" src="flappybird/js/main.js"></script>
      <link rel="stylesheet" href="kirjaudu.css">
   </head>
   <!--Body-->
   <body class="preload">
      <!--Container sisältää skrollattavan osion eli tällä sivustolla kaiken sisällön-->
      <div class="container">
         <input type="radio" name="radio-set"  id="st-control-2" onclick="transformDown()"/>
         <a id="st-control-2a" href="#st-panel-2"><img src="arrow-up-128.png" alt="Go down" class="alaskuva"></a>
         <!--Mainscroll sisältää paneelit jotka liikkuvat skrollattaessa-->
         <div id="mainscroll" class="st-scroll">
            <section class="st-panel" id="st-panel-1">
               <img src="4180159-winter-in-finland2.jpg" class="kirjaudukuva" alt="">
               <!--Kirjautumislomake etusivulla-->
               <form action="?action=send" method="post" id="1stForm"  class="kirjauduform">
                  <fieldset>
                     <input type="text" id="user" name="user" class="kayttajanimi" placeholder="User" maxlength="10">
                  </fieldset>
                  <fieldset>
                     <input type="password" id="password" name="password" class="salasana" placeholder="Password" maxlength="10">
                  </fieldset>
                  <button type="button" onclick="showUser()" id="Send" class="kirjauduNappi">Log in</button>
               </form>
            </section>
            <!--Ensimmäinen sivu päättyy-->
            <!--Toinen sivu alkaa-->
            <section class="st-panel" id="st-panel-2">
               <!--Kirjautumislomake toisella sivulla-->
               <div id="kirjauduUp">
                  <form action="?action=send" method="post" id="2ndForm" class="kirjauduform2" >
                     <fieldset>
                        <input type="text" id="user2" name="user" class="kayttajanimi" placeholder="User" maxlength="10">
                        <input type="password"  id="password2" name="password" class="salasana" placeholder="Password" maxlength="10">
                        <button type="button" onclick="showUser()" id="Send2" class="kirjauduNappi2">Log in</button>
                     </fieldset>
                  </form>
                  <!--Log out nappi-->
                  <div id="userdataContainer">
                     <div id="usr">test</div>
                     <div id="personalScore"></div>
                  </div>
                  <button type="button" onclick="logOut()" id="logOut">Log out</button>
                  <div id="upButton"  class="upButton">
                     <input type="radio" name="radio-set" checked="checked" id="st-control-1" onclick="transformDown()"/>
                     <a id="st-control-1a" href="#st-panel-1"> <img src="arrow-up-128.png" class="yloskuva" alt="Go up"></a>
                  </div>
                  <!--Highscoret tulevat tähän osioon-->
                  <div id="highscore">
                  </div>
               </div>
               <h1 id="flappyTitle">Xmas Bird</h1>
               <div id="description">
                  <p id="descriptionText">Help the Xmas bird to fly through the trees without touching them.</p>
                  <p>Game starts with spacebar or mouse-click.
                     Bird flies higher when pressing spacebar or mouse-click and falls down without your help.
                  </p>
                  <p>By logging in you can save your maximun score and even get to the top 5 list.</p>
               </div>
               <div class="st-deco" data-icon="H"></div>
               <div id="bird">
                  <!--tähän tulee peli, scripti on vaa tuol ylhääl-->
               </div>
            </section>
         </div>
      </div>
   </body>
</html>



<?php
    class Modelgame {
      
        
        //Funktio lisää käyttäjän tietokantaan
        
        public function add_user($user, $password) {
            
            $servername = "localhost";
            $username = "root";
            $sqlpassword = "";
            $dbname = "peli_db";

            // Create connection
            $conn = new mysqli($servername, $username, $sqlpassword, $dbname);
            // Check connection
            if ($conn->connect_error) {
             die("Connection failed: " . $conn->connect_error);
            } 

            $sql = "INSERT INTO Users (idUsers, password)
            VALUES ('$user', '$password')";

            if ($conn->query($sql) === TRUE) {
             
            } else {
             echo "Error: " . $sql . "<br>" . $conn->error;
            }

            $conn->close();
         }
         
         //Funktio lisää pisteet
         
         public function add_score($user, $score) {
            
            $servername = "localhost";
            $username = "root";
            $sqlpassword = "";
            $dbname = "peli_db";

            // Create connection
            $conn = new mysqli($servername, $username, $sqlpassword, $dbname);
            // Check connection
            if ($conn->connect_error) {
             die("Connection failed: " . $conn->connect_error);
            } 

            $sql = "UPDATE Users SET score='$score' WHERE idUsers='$user'";

            if ($conn->query($sql) === TRUE) {
             //echo "New record created successfully";
            } else {
             echo "Error: " . $sql . "<br>" . $conn->error;
            }

            $conn->close();
         }
         
         //Funktio hakee käyttäjän tietokannasta $user id:llä
         
         public function get_user($user){
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "peli_db";

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            } 
        
            // $sql = "SELECT * FROM Users WHERE idUsers='$user'";
            // $result = $conn->query($sql);
    
    
            $stmt = $conn->prepare("SELECT * FROM Users WHERE idUsers=?");
            $stmt->bind_param("s",$user);
            $stmt->execute();
            $result = $stmt->get_result();
           
              $stmt->close();

            if ($result->num_rows > 0) {
                
                $jsonarray = json_encode($result -> fetch_assoc());
                $conn->close();
                return $jsonarray;
                
            }  else {
            return array();
            $conn->close();
         }
         }
            
            
            
        
        //Funktio hakee tietokannasta käyttäjän $user pisteet
            
        public function get_score($user){
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "peli_db";

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            } 

            $sql = "SELECT score FROM Users WHERE idUsers='$user'";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
            // output data of each row
            $jsonarray = json_encode($result -> fetch_assoc());
            $conn->close();
            return $jsonarray;
            
            
            } else {
            echo "0 results";
            $conn->close();
            }
                
            }
        
        //Funkio hakee tietokannasta kaikkien käyttäjien pisteet ja sorttaa ne korkeimmasta alkaen
            
        public function get_scores(){
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "peli_db";

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            } 

            $sql = "SELECT idUsers, score FROM Users ORDER BY score DESC LIMIT 5";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                $i=1;
            echo "<table><tr><th>Scores</th></tr>";
            // output data of each row
            while($row = $result->fetch_assoc()) {
            echo "<tr><td>$i</td><td>".$row["idUsers"]."</td><td>".$row["score"]."</td></tr>";
            $i++;
            }
            echo "</table>";
            } else {
            echo "0 results";
            }
            $conn->close();    
            }
    }
    
    
    ?>
Xmas Games -program version 1.0 12/2016

General information:
This site offers nice flappy bird -game with possibility to log in and save top score.
Run Logingame.php to use the site and create database with kokeilu.sql.

Requirements:
Supports web-kit and mozilla browsers. Supports also older browsers without css support. 

Contact information:
Group 10: Otso, Lauri, Tiia
Metropolia University of applied sciences
Information and communications technology

Notices:
Some problems occured with safari browser support and some minor visual problems can occur also
with some other browsers.
Ajax requests are carried out with get-methods. Correct way is to use post-methods, but at this version
it was not possible.
There may be also some other security related problems, which should be fixed at next version.
Unit testing should have been done, but this time it was not possible due to lack of time.

-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema peli_db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema peli_db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `peli_db` DEFAULT CHARACTER SET utf8 ;
USE `peli_db` ;

-- -----------------------------------------------------
-- Table `peli_db`.`Users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `peli_db`.`Users` (
  `idUsers` VARCHAR(20) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `score` INT NULL,
  PRIMARY KEY (`idUsers`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

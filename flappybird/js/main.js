var game = new Phaser.Game(400, 490, Phaser.AUTO, "bird");

var mainState = {

    preload: function() { 
        
        if(!game.device.desktop) {
            game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            game.scale.setMinMax(game.width/2, game.height/2, game.width, game.height);
        }
        
        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;

        game.stage.backgroundColor = '#71c5cf';
        
        // Load images
        game.load.image('bird', 'flappybird/assets/bird.png');  
        game.load.image('pipe', 'flappybird/assets/pipe.png'); 
        
        // Load sounds
        game.load.audio('music', 'flappybird/assets/music.wav');
        game.load.audio('quack', 'flappybird/assets/quack.wav');
        
    },

    create: function() {
        
        game.physics.startSystem(Phaser.Physics.ARCADE);
        
        this.pipeVelocity = -200;
        
        this.pipes = game.add.group();
        this.timer = game.time.events.loop(1500, this.addRowOfPipes, this);           

        this.bird = game.add.sprite(100, 245, 'bird');
        game.physics.arcade.enable(this.bird);
        this.bird.body.gravity.y = 1000; 

        // New anchor position
        this.bird.anchor.setTo(-0.2, 0.5); 
 
        // Jump key
        var spaceKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        spaceKey.onDown.add(this.jump, this); 
        game.input.onDown.add(this.jump, this);
        
        
        this.score = 0;
        this.labelScore = game.add.text(20, 20, "0", { font: "30px Arial", fill: "#ffffff" });
        
        // Add the quack sound
        this.quackSound = game.add.audio('quack');
        this.quackSound.volume = 0.5;
        
        // Add the game music
        this.musicSound = game.add.audio('music');
        this.musicSound .volume = 0.2;
        this.musicSound.play();
        
        // Pause the game when it starts
        this.pause();
        
    },

    update: function() {
        
        // Detects when the bird is off the screen.
        if (this.bird.y < 0 || this.bird.y > game.world.height) {
            this.restartGame();
            this.musicSound.stop();
        }
        
        // Detects when the bird hits a pipe.
        game.physics.arcade.overlap(this.bird, this.pipes, this.hitPipe, null, this); 
            
        // Slowly rotate the bird downward, up to a certain point.
        if (this.bird.angle < 20)
            this.bird.angle += 1; 
            
    },
    
    pause: function() {
        game.paused = true;
        
    },

    jump: function() {
        game.paused = false;
        // If the bird is dead, he can't jump
        if (this.bird.alive == false)
            return; 

        this.bird.body.velocity.y = -350;

        // Jump animation
        game.add.tween(this.bird).to({angle: -20}, 100).start();

    },

    hitPipe: function() {
        // If the bird has already hit a pipe, we have nothing to do
        if (this.bird.alive == false)
            return;
            
        // Set the alive property of the bird to false
        this.bird.alive = false;
        this.musicSound.stop();
        this.quackSound.play();
        // Prevent new pipes from appearing
        game.time.events.remove(this.timer);
    
        // Go through all the pipes, and stop their movement
        this.pipes.forEach(function(p){
            p.body.velocity.x = 0;
        }, this);
    },

    restartGame: function() {
        //handles the hiscores
        showScore(this.score);
        
        //starts over
        game.state.start('main');
    },

    addOnePipe: function(x, y) {

        var pipe = game.add.sprite(x, y, 'pipe');
        this.pipes.add(pipe);
        game.physics.arcade.enable(pipe);

        pipe.body.velocity.x = this.pipeVelocity;  
        pipe.checkWorldBounds = true;
        pipe.outOfBoundsKill = true;
    },

    addRowOfPipes: function() {
        var hole = Math.floor(Math.random()*5)+1;
        
        for (var i = 0; i < 8; i++)
            if (i != hole && i != hole +1) 
                this.addOnePipe(400, i*60+10);   
    
        this.score += 1;
        this.labelScore.text = this.score;  
    },
};

game.state.add('main', mainState);  
game.state.start('main'); 